# CurTec

Curtec is a web service for interactive learning through video tutorials of students for students.

## Getting Started

Running the web service test
```
Pos' le pushas en correr con glassfish y ya we :v
```

### Main dependencies

#### Frontend
* Bootstrap
* jQuery
* PopperJS
* ChartJS
* FontAwesome

#### Backend
* Spring
* JavaEE
* MariaDB

## Authors

### Original Authors and Development Lead

**Mauricio Reyes** - [Gitlab](https://gitlab.com/Rezamar)

**César Rivera** - [Gitlab](https://gitlab.com/cesar.rivera.sc)

### Fullstack developers

**Ernesto Ruíz** - [Gitlab](https://gitlab.com/senjuana)

**Azael Rodríguez** - [Gitlab](https://gitlab.com/azael_rguez)

## Licence

This project is licensed under the MIT License - see the [LICENSE](LICENSE) file for details.

Documentation is licensed under [CC BY-NC-ND 4.0](BY-NC-ND.md).
