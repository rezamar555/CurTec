@extends('cabecera')
@section('contenido')
<div id="espacioblanco">
	<div class="container principal">
		<center>
			<div class="jumbotron">
				<h1>¿Qué es CurTec?</h1><br>
				<h2>Es una plataforma Intuitiva de Vídeo Cursos que fomenta el estudio y la resolución de dudas
				respecto a asignaturas de las actuales carreras.</h2>
				
				<br><h3>Con la iniciativa de aprendizaje, y siendo una alternativa ante las UAS, se enfocan Vídeo Cursos 
				realizados por la Comunidad Estudiantil para la misma, de los Institutos Tecnológicos.</h3>
				<!--<i class="fa fa-book fa-5x" aria-hidden="true"></i> -->
			</div>
		</center>
	</div>
</div>
<div id="central">
	<div class="container principal">
		<center>
			<div class="jumbotron">
				<h2>¿Quiéres disfrutar de todos los benefios?</h2><br>
				<a role="button" href="{{'Reg'}}" class="btn btn-primary btn-lg">Regístrate aquí</a>
			</div>
		</center>
	</div>
</div>
@stop
