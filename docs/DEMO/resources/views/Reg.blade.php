@extends('cabecera')
@section('contenido')	
<div id="espacioblanco">
	<div class="container principal">
		<div class="row">
			<div class="jumbotron col-md-6 col-md-offset-3">
				<form action="{{url('callReg')}}" method="post">
				{{ csrf_field() }}
					
					<div class="form-group">
						<label for="input-nombre">Nombre</label>
						<input type="text" name="input-nombre" class="form-control" placeholder="Ejemplo: Usuario" required>
					</div>
					<div class= "form-group">
						<label for="input-apellido">Apellido</label>
						<input type="text" name="input-apellido" class="form-control" placeholder="Ejemplo: Usuario" required>
					</div>					
					<div class= "form-group">
						<label for="input-email">Correo Institucional</label>
						<input type="email" name="input-email" class="form-control" placeholder="Ejemplo: usuario.sc@itszapopan.edu.mx" required="@itszapopan.edu.mx">
					</div>
					<div class= "form-group">
						<label for="input-noControl">Número de Control</label>
						<input type="text" name="input-noControl" class="form-control" placeholder="Ejemplo: 15150120" required>
					</div>
					<div class= "form-group">
						<label for="input-contraseña">Contraseña</label>
						<input type="password" name="input-contraseña" class="form-control" placeholder="Ejemplo: ************" required>
					</div>
					<input class="btn btn-default" type="submit" value="Limpiar"/>
					<input class="btn btn-primary" type="submit" value="Enviar"/>
					<h6>Si tiene una cuenta ingrese <a href="{{'Login'}}">Aquí</a></h6>
					
				</form>
			</div>
		</div>
	</div>
</div>
@stop
