@extends('cabecera')
@section('contenido')		
<div id="espacioblanco">
	<div class="container principal">
		<div class="row">
			<div class="jumbotron col-md-6 col-md-offset-3">
				<i class="fa fa-times-circle fa-5x" aria-hidden="true"></i>
				<form action="Procesar.php" method="post" name="frm">
					
					<h2>Registro Fallido!</h2>
					<h3>La información ingresada ya esta registrada</h3>
					<h3>{{$n}}</h3>
					<a class="btn btn-primary" href="{{'Reg'}}" role="button">Regresar</a>					
				</form>
			</div>
		</div>
	</div>
</div>
@stop
