@extends('cabecera')
@section('contenido')		
<div id="espacioblanco">
	<div class="container principal">
		<div class="row">
			<div class="jumbotron col-md-6 col-md-offset-3">
				<form action="{{url('callLog')}}" method="post" name="frm">
				{{ csrf_field() }}
					<div class="form-group">
						<label for="input-email">Correo Institucional</label>
						<input type="email" name="input-email" class="form-control" placeholder="Ejemplo: usuario@itszapopan.edu.mx" required>
					</div>
					<div class= "form-group">
						<label for="input-contraseña">Contraseña</label>
						<input type="password" name="input-contraseña" class="form-control" placeholder="Ejemplo: ************" required>
					</div><br>
					<input class="btn btn-primary" type="submit" value="Ingresar"/>
					<div>
					<h6>Si no tiene una cuenta de clic<a href="{{'Reg'}}">Aquí</a></h6>
					
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
@stop
